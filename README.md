## Java Multithreading - Fork/Join Framework - Blur Image Tool

This is a simple application written in Java to demonstrate computation using multiple cores. It uses the Fork/Join framework present within `java.util.concurrent` package. The program uses java swing for the application's frontend.

## Description

The program  implements a simple horizontal image blur. It averages pixels in the 
source array and writes them to a destination array. A threshold value 
determines whether the blurring will be performed directly or split into two tasks.

## Build

Requires `JDK 1.8` or higher

1. `cd` into `src` directory

        cd ./src

2. Compile with `javac`

         javac BlurImageFrame.java DisplayImageFrame.java ForkBlur.java Image.java

## Run

1. Run `BlurImageFrame.class` with `java`

        java BlurImageFrame