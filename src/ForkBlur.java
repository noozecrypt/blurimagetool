/*
 * Copyright (c) 2010, 2013, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.awt.image.BufferedImage;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;


/**
 * ForkBlur implements a simple horizontal image blur. It averages pixels in the
 * source array and writes them to a destination array. The sThreshold value
 * determines whether the blurring will be performed directly or split into two
 * tasks.
 *
 * This is not the recommended way to blur images; it is only intended to
 * illustrate the use of the Fork/Join framework.
 */
public class ForkBlur extends RecursiveAction {

    private int[] mSource;
    private int mStart;
    private int mLength;
    private int[] mDestination;

    private static int mBlurWidth = 15; // Processing window size, should be odd.
    private static int sThreshold = 10_000;
    private static long timeTakenToBlurImage;
    private static boolean isMultiThreadingOn;


    ForkBlur(int[] src, int start, int length, int[] dst) {
        mSource = src;
        mStart = start;
        mLength = length;
        mDestination = dst;
    }

    // Average pixels from source, write results into destination.
    void computeDirectly() {
        int sidePixels = (mBlurWidth - 1) / 2;
        for (int index = mStart; index < mStart + mLength; index++) {
            // Calculate average.
            float red = 0, green = 0, blue = 0;
            for (int sidePixelIndex = -sidePixels; sidePixelIndex <= sidePixels; sidePixelIndex++) {

                //To make sure the sidePixels does not create ArrayIndexOutOfBoundException.
                int validSidePixelIndex = Math.min(Math.max(sidePixelIndex + index, 0), mSource.length - 1);

                int pixel = mSource[validSidePixelIndex];
                red += (float) ((pixel & 0x00ff0000) >> 16) / mBlurWidth;
                green += (float) ((pixel & 0x0000ff00) >> 8) / mBlurWidth;
                blue += (float) ((pixel & 0x000000ff) >> 0) / mBlurWidth;
            }

            // Re-assemble destination pixel.
            int destinationPixel = (0xff000000)
                    | (((int) red) << 16)
                    | (((int) green) << 8)
                    | (((int) blue) << 0);
            mDestination[index] = destinationPixel;
        }
    }


    @Override
    protected void compute() {
        if (mLength < sThreshold) {
            computeDirectly();
            return;
        }

        int split = mLength / 2;

        invokeAll(new ForkBlur(mSource, mStart, split, mDestination),
                new ForkBlur(mSource, mStart + split, mLength - split,
                        mDestination));
    }

    static BufferedImage blur(BufferedImage sourceImage) {
        int srcImageWidth = sourceImage.getWidth();
        int srcImageHeight = sourceImage.getHeight();

        // getRGB() returns an array of integer pixels in the default RGB color model(TYPE_INT_ARGB)
        // and default sRGB colour space, from a portion of the image data.
        int[] sourceRGBPixelsArray = sourceImage.
                getRGB(0, 0, srcImageWidth, srcImageHeight, null, 0, srcImageWidth);
        int[] destinationRGBPixelsArray = new int[sourceRGBPixelsArray.length];


        //Fork Blur - Recursive Action to blur the image using divide and conquer approach.
        ForkBlur fb = new ForkBlur(sourceRGBPixelsArray, 0, sourceRGBPixelsArray.length, destinationRGBPixelsArray);

        if (isIsMultiThreadingOn())
            multiThreadedBlur(fb);
        else
            singleThreadedBlur(fb);

        BufferedImage destinationImage =
                new BufferedImage(srcImageWidth, srcImageHeight, BufferedImage.TYPE_INT_ARGB);

        destinationImage
                .setRGB(0, 0, srcImageWidth, srcImageHeight, destinationRGBPixelsArray, 0, srcImageWidth);

        return destinationImage;
    }

    private static void singleThreadedBlur(ForkBlur fb) {
        long startTime = System.currentTimeMillis();
        fb.computeDirectly();
        long endTime = System.currentTimeMillis();

        timeTakenToBlurImage =  endTime - startTime;
    }

    private static void multiThreadedBlur(ForkBlur fb) {
        ForkJoinPool pool = new ForkJoinPool();

        long startTime = System.currentTimeMillis();
        pool.invoke(fb);
        long endTime = System.currentTimeMillis();

        timeTakenToBlurImage =  endTime - startTime;
    }

    static void setmBlurWidth(int mBlurWidth) {
        ForkBlur.mBlurWidth = mBlurWidth;
    }

    static void setsThreshold(int sThreshold) {
        ForkBlur.sThreshold = sThreshold;
    }

    static String getmBlurWidth() {
        return String.valueOf(mBlurWidth);
    }

    static String getsThreshold() {
        return String.valueOf(sThreshold);
    }

    static String getTimeTakenToBlurImage() {
        return String.valueOf(timeTakenToBlurImage);
    }

    static String getProcessorCount() {
        return String.valueOf(Runtime.getRuntime().availableProcessors());
    }

    static boolean isIsMultiThreadingOn() {
        return isMultiThreadingOn;
    }

    static void setIsMultiThreadingOn(boolean isMultiThreadingOn) {
        ForkBlur.isMultiThreadingOn = isMultiThreadingOn;
    }
}
