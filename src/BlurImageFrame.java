import javax.swing.*;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class BlurImageFrame extends JFrame {


    public static final String APP_TITLE = "Image Blur Tool ( ͡° ͜ʖ ͡°)";

    JButton loadImageButton, blurImageButton;
    JLabel displayImageLabel, titleLabel, chooseImageLabel, blurWidthLabel, thresholdLabel, threadingLabel,
        imageResLabel, imagePixelCountLabel, noOfProcessorsLabel, timeElapsedLabel, timeElapsedValueLabel,
        imageResValueLabel, imagePixelCountValueLabel, noOfProcessorValueLabel;

    JRadioButton threadingOnRadioButton, threadingOffRadioButton;

    ButtonGroup threadingButtonGroup;

    DefaultComboBoxModel<Image> listOfPictures;

    JComboBox<Image> selectImageComboBox;

    JScrollPane selectImagesScrollPane;

    JTextField blurWidthTextField, thresholdTextField;

    public static void main(String[] args) throws IOException {
        new BlurImageFrame(APP_TITLE);
    }

    public BlurImageFrame(String title) throws HeadlessException {
        super(title);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLayout(null);

        //initializing Buttons
        loadImageButton = new JButton("Load Image");
        blurImageButton = new JButton("Blur");

        //initializing RadioButtons
        threadingOnRadioButton = new JRadioButton("On");
        threadingOffRadioButton = new JRadioButton("Off");
        threadingButtonGroup = new ButtonGroup();

        //initializing Labels
        titleLabel = new JLabel("Image Blur Tool");
        chooseImageLabel = new JLabel("Choose Image to Blur");
        blurWidthLabel = new JLabel("Blur Width");
        thresholdLabel = new JLabel("Threshold");
        threadingLabel = new JLabel("Threading");
        imageResLabel = new JLabel("Image Resolution");
        imagePixelCountLabel = new JLabel("Image Pixel Count");
        noOfProcessorsLabel = new JLabel("No of Processors");
        timeElapsedLabel = new JLabel("Time Elapsed");
        timeElapsedValueLabel = new JLabel("Time Elapsed Value Label");
        displayImageLabel = new JLabel();

        imageResValueLabel = new JLabel();
        imagePixelCountValueLabel = new JLabel();
        noOfProcessorValueLabel = new JLabel();
        timeElapsedValueLabel = new JLabel();

        //initializing TextFields
        blurWidthTextField = new JTextField();
        thresholdTextField = new JTextField();

        //initialize ComboBoxModel
        listOfPictures = new DefaultComboBoxModel();

        listOfPictures.addElement(Image.PIC_1);
        listOfPictures.addElement(Image.PIC_2);
        listOfPictures.addElement(Image.PIC_3);
        listOfPictures.addElement(Image.PIC_4);
        listOfPictures.addElement(Image.PIC_5);
        listOfPictures.addElement(Image.PIC_6);
        listOfPictures.addElement(Image.PIC_7);
        listOfPictures.addElement(Image.PIC_8);
        listOfPictures.addElement(Image.PIC_9);
        listOfPictures.addElement(Image.PIC_10);


        //initialize JComboBox
        selectImageComboBox = new JComboBox<>(listOfPictures);
        selectImageComboBox.setFont(new Font("Arial",NORMAL,48));
        selectImageComboBox.setPreferredSize(new Dimension(300,80));
        selectImageComboBox.setMaximumRowCount(4);
        selectImageComboBox.setSelectedIndex(0);
        //add RadioButton to ButtonGroup
        threadingButtonGroup.add(threadingOffRadioButton);
        threadingButtonGroup.add(threadingOnRadioButton);

        //initialize JScrollPane
        selectImagesScrollPane = new JScrollPane(selectImageComboBox);

        setSize(2250, 1500);

        threadingOnRadioButton.setSelected(true);
        noOfProcessorValueLabel.setText(ForkBlur.getProcessorCount());
        thresholdTextField.setText("10000");
        blurWidthTextField.setText("15");


        //add ActionListeners
        loadImageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    selectImage();
                    loadImage();
                    imagePixelCountValueLabel.setText(String.valueOf(imagePixelCount));
                    imageResValueLabel.setText(imageResolution);
                    new DisplayImageFrame(originalImage, "Original Image" + " - " + image.getImageName());
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        blurImageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ForkBlur.setIsMultiThreadingOn(threadingOnRadioButton.isSelected());
                ForkBlur.setmBlurWidth(Integer.parseInt(blurWidthTextField.getText()));
                ForkBlur.setsThreshold(Integer.parseInt(thresholdTextField.getText()));
                blurImage();
                new DisplayImageFrame(blurredImage, "Blurred Image" + " - " + image.getImageName());
                timeElapsedValueLabel.setText(ForkBlur.getTimeTakenToBlurImage() + " ms");
            }
        });

        titleLabel.setBounds(900,100,400,100);
        chooseImageLabel.setBounds(50,300,1000,100);
        loadImageButton.setBounds(1600,300,400,100);
        blurWidthLabel.setBounds(100,600,400,100);
        blurWidthTextField.setBounds(600,600,200,100);
        blurImageButton.setBounds(900,700,300,100);
        imageResLabel.setBounds(1300,600,500,100);
        imageResValueLabel.setBounds(1900,600,250,100);
        imagePixelCountLabel.setBounds(1300,800,500,100);
        imagePixelCountValueLabel.setBounds(1900,800,250,100);
        noOfProcessorsLabel.setBounds(1300,1000,500,100);
        noOfProcessorValueLabel.setBounds(1900,1000,250,100);
        thresholdLabel.setBounds(100,800,400,100);
        thresholdTextField.setBounds(600,800,200,100);
        threadingLabel.setBounds(100,1000,400,100);
        threadingOnRadioButton.setBounds(600,1000,100,100);
        threadingOffRadioButton.setBounds(800,1000,100,100);
        timeElapsedLabel.setBounds(700, 1200,400,100);
        timeElapsedValueLabel.setBounds(1100, 1200,400, 100);
        selectImagesScrollPane.setBounds(800,300,600,100);


        //setFont
        titleLabel.setFont(new Font("Arial",NORMAL,48));
        chooseImageLabel.setFont(new Font("Arial",NORMAL,48));
        loadImageButton.setFont(new Font("Arial",NORMAL,48));
        displayImageLabel.setFont(new Font("Arial",NORMAL,48));
        blurWidthLabel.setFont(new Font("Arial",NORMAL,48));
        blurWidthTextField.setFont(new Font("Arial",NORMAL,48));
        imageResLabel.setFont(new Font("Arial",NORMAL,48));
        imageResValueLabel.setFont(new Font("Arial",NORMAL,48));
        blurImageButton.setFont(new Font("Arial",NORMAL,48));
        imagePixelCountLabel.setFont(new Font("Arial",NORMAL,48));
        imagePixelCountValueLabel.setFont(new Font("Arial",NORMAL,48));
        noOfProcessorsLabel.setFont(new Font("Arial",NORMAL,48));
        noOfProcessorValueLabel.setFont(new Font("Arial",NORMAL,48));
        thresholdLabel.setFont(new Font("Arial",NORMAL,48));
        thresholdTextField.setFont(new Font("Arial",NORMAL,48));
        threadingLabel.setFont(new Font("Arial",NORMAL,48));
        threadingOnRadioButton.setFont(new Font("Arial",NORMAL,48));
        threadingOffRadioButton.setFont(new Font("Arial",NORMAL,48));
        timeElapsedValueLabel.setFont(new Font("Arial",NORMAL,48));
        timeElapsedLabel.setFont(new Font("Arial",NORMAL,48));

        //add Views to JFrame
        add(titleLabel);
        add(chooseImageLabel);
        add(loadImageButton);
        add(displayImageLabel);
        add(blurWidthLabel);
        add(blurWidthTextField);
        add(blurImageButton);
        add(imageResLabel);
        add(imageResValueLabel);
        add(imagePixelCountLabel);
        add(imagePixelCountValueLabel);
        add(noOfProcessorsLabel);
        add(noOfProcessorValueLabel);
        add(thresholdLabel);
        add(thresholdTextField);
        add(threadingLabel);
        add(threadingOffRadioButton);
        add(threadingOnRadioButton);
        add(timeElapsedLabel);
        add(timeElapsedValueLabel);
        add(selectImagesScrollPane);


        setVisible(true);
    }

    private static void run() throws IOException {
        BlurImageFrame blurImageFrame= new BlurImageFrame(APP_TITLE);
        blurImageFrame.selectImage();
        blurImageFrame.loadImage();
        ForkBlur.setIsMultiThreadingOn(false);
        blurImageFrame.blurImage();
        System.out.println(ForkBlur.getTimeTakenToBlurImage());
        blurImageFrame.writeImage(blurImageFrame.blurredImage);
    }


    private Image image = Image.PIC_1;

    private BufferedImage originalImage;
    private BufferedImage blurredImage;

    private String imageWidth;
    private String imageHeight;
    private long imagePixelCount;
    private String imageResolution;


    // will be used by the ListListener to choose the image to be loaded.
    private void selectImage(){
        this.image = selectImageComboBox.getItemAt(selectImageComboBox.getSelectedIndex());
    }



    private void loadImage() throws IOException {
        originalImage = image.getBufferedImage();
        imageWidth = String.valueOf(originalImage.getWidth());
        imageHeight = String.valueOf(originalImage.getHeight());
        imagePixelCount = (long)originalImage.getHeight() * originalImage.getWidth();
        imageResolution = imageWidth + "X" + imageHeight;
    }

    private void blurImage(){

        blurredImage = ForkBlur.blur(originalImage);

    }

    private void writeImage(BufferedImage blurredImage) throws IOException {
        image.writeBlurredImage(blurredImage);
    }

}
