import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public enum Image {
    PIC_1("Eiffel Tower","pic1.jpg"),
    PIC_2("Sea Shore", "pic2.jpg"),
    PIC_3("Ice Mountain", "pic3.jpg"),
    PIC_4("Hot Air Balloon", "pic4.jpg"),
    PIC_5("Beach Landscape","pic5.jpg"),
    PIC_6("Lighthouse","pic6.jpg" ),
    PIC_7("Beach Shore Rocks","pic7.jpg"),
    PIC_8("Resort", "pic8.jpg"),
    PIC_9("Sierra","pic9.jpg"),
    PIC_10("Landscape Reflection","pic10.jpg");


    private final String imageName;
    private final String imagePath;
    private final String blurredImagePath;

    private static final String imageBasePath = new File("").getAbsolutePath().concat("/assets");


    Image(String imageName, String imagePath) {
        this.imageName = imageName;
        this.imagePath = imagePath;
        this.blurredImagePath = imageName.concat("-blurred.jpg");
    }

    BufferedImage getBufferedImage() throws IOException {
        File file = new File(imageBasePath,imagePath);
        return ImageIO.read(file);
    }

    public String getImageName() {
        return imageName;
    }

    void writeBlurredImage(BufferedImage image) throws IOException {
        File file = new File(imageBasePath,blurredImagePath);
        ImageIO.write(image,"jpg",file);
    }

    @Override
    public String toString() {
        return getImageName();
    }
}
