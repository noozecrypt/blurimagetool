import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public class DisplayImageFrame extends JFrame {


    BufferedImage bufferedImage;

    JLabel displayImageLabel;

    public DisplayImageFrame(BufferedImage bufferedImage,String frameTitle) throws HeadlessException {
        super(frameTitle);
        this.bufferedImage = bufferedImage;

        setLayout(null);

        displayImageLabel = new JLabel();
        displayImageLabel.setIcon(new ImageIcon(bufferedImage));

        displayImageLabel.setBounds(50, 50,1920,1080);

        add(displayImageLabel);



        setSize(2050,1300);
        setVisible(true);
    }


}
